import QtQuick 2.0
import Sailfish.Silica 1.0

CoverBackground {
    anchors.fill: parent
    Label {
        anchors.centerIn: parent
        text: "Weight Tracker"
    }
}
