# Weight Tracker for SailfishOS

A weight tracker for SailfishOS.

# Authors

This project was built during a project at the Université de Pau et des Pays de l'Adour (France), by the following students and under the supervision of Adel Noureddine:
- Manon Sauvageot
- Yoann Dumont
- Louis Puyou

# License

The project is licensed under the GNU GPL v3.
